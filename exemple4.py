#!/usr/bin/python

from matplotlib.pyplot import *
from numpy.random import *

def convert(x, y):
	data = []
	for i in range(len(x)-1):
		data = data + ([x[i]]*(y[i]/(x[i+1]-x[i])))
	return data

bins = [1,2,3,5,10]

x = ([1]*56)+([2]*68)+([3]*(35/2))+([5]*(41/5))

hist(x, bins)
ylabel("Hauteur en m")

title("Nombre d'abres en fonction de leur hauteur")
show()