#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import sqrt, exp
from matplotlib.pyplot import *
from numpy.random import *
from numpy import *
import pickle

data = pickle.load(open('data5.txt', 'r'))

taille_tableau = data.shape

print('Taille de l\'échantillon: %s' % str(taille_tableau))

l = data[:,0]
w = data[:,1]

moy_l = sum(l) / float(len(l))

variance_l = (sum(l ** 2) / float(taille_tableau[0])) - moy_l ** 2
ecart_type_l = sqrt(variance_l)

moy_w = sum(w) / float(len(w))

variance_w = (sum(w ** 2) / float(taille_tableau[0])) - moy_w ** 2
ecart_type_w = sqrt(variance_w)

print('Moyenne longueur: %f' % moy_l)
print('Moyenne largeur: %f' % moy_w)

covariance = (sum(l*w) / float(data.shape[0])) - (moy_l * moy_w)

print('Covariance: %f' % covariance)

reg = covariance / (ecart_type_l * ecart_type_w)

print('Coefficient de corrélation linéaire: %f' % reg)

a = float(covariance) / (ecart_type_l ** 2)

droite_reg = lambda x: a*x + (moy_w - a * moy_l)


scatter(l, w)

x = arange(min(l), max(l))

xlabel('longueur du nez')
ylabel('largeur du nez')

title('Kangourous')
plot(x, droite_reg(x))
show()