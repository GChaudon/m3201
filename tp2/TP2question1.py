#!/usr/bin/env python
# -*- coding: utf-8

from math import sqrt, exp
from matplotlib.pyplot import *
from numpy.random import *
from numpy import *
import pickle

x = pickle.load(open('data2.txt', 'r'))
x=x[:-10]
nb_communes = x.shape[0]

print('Nombre de communes en France: %d' % nb_communes)

print('Valeur minimale : %d' % min(x))
print('Valeur maximale: %d' % max(x))

print('Nombre d\'habitants: %d' % sum(x))

moyenne = sum(x) / float(nb_communes)

variance = (sum(x ** 2) / float(nb_communes)) - moyenne ** 2
ecart_type = sqrt(variance)

sorted_x = sort(x)
mediane = sorted_x[sorted_x.shape[0] / 2]

print('Moyenne: %f' % moyenne)
print('Médiane : %f' % mediane)
print('Écart-type: %f' % ecart_type)

mu = moyenne
sigma = ecart_type

gaussienne = lambda x: (1 / float(sigma * sqrt(2*pi))) * exp(-.5 * (((x - mu) / sigma)) ** 2)

bins = arange(200000)

# Pas du tout bien approche par une gaussienne
# Ecart avec les valeurs

plot(bins, gaussienne(bins))
hist(x, normed=1,bins=50)
show()
