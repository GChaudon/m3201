#!/usr/bin/python
# -*- coding: utf-8 -*-

from numpy import *
from matplotlib.pyplot import *

data = array([
	[27, 6.8],
	[43, 20.5],
	[62, 35.9],
	[80, 67.8],
	[98, 101.2],
	[115, 135.8]
])

x = data[:,0] ** 2
y = data[:,1]

# y = y ** 2

mx = sum(x) / x.shape[0]
variance_x = (sum(x**2) / x.shape[0]) - mx ** 2
ecart_type_x = sqrt(variance_x)

print('Moyenne de x: %f' % mx)
print('Variance de x: %.2f' % variance_x)

my = sum(y) / y.shape
variance_y = (sum(y**2) / y.shape) - my ** 2
ecart_type_y = sqrt(variance_y)

print('Moyenne de y: %f' % my)
print('Variance de y: %.2f' % variance_y)

cov = (sum(x*y) / float(x.shape[0])) - mx * my

print('Cov(X, Y) = %.2f' % cov)

reg = cov / (ecart_type_x * ecart_type_y)

print('Coefficient de corrélation linéaire: %f' % reg)

a = float(cov) / (ecart_type_x ** 2)
b = my - a*mx
droite_reg = lambda x: a*x + b

print('y = %fx + %f' % (a, b))

print('Distance d\'arrêt de 180m ==> %.2f km/h' % (sqrt((180 - b) / a)))

# Point moyen
scatter(mx, my, color='red')
scatter(x, y)

r = arange(min(x), max(x))

plot(r, droite_reg(r), color='red')

title('Vitesse en fonction de la distance d\'arret')
xlabel('Carre de la vitesse (en km2/h)')
ylabel('Distance d\'arret (en m)')
show()