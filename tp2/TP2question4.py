#!/usr/bin/env python
# -*- coding: utf-8

from math import sqrt, exp
from matplotlib.pyplot import *
from numpy.random import *
from numpy import *
import pickle

x = pickle.load(open('data3.txt', 'r'))

nb_mois = x.shape[0]

moyenne = sum(x) / float(nb_mois)

variance = (sum(x ** 2) / float(nb_mois)) - moyenne ** 2
ecart_type = sqrt(variance)

sorted_x = sort(x)
mediane = sorted_x[sorted_x.shape[0] / 2]

print('Moyenne: %f' % moyenne)
print('Médiane : %f' % mediane)
print('Écart-type: %f' % ecart_type)

mu = moyenne
sigma = ecart_type

gaussienne = lambda x: (1 / (sigma * sqrt(2*pi))) * exp(-.5 * (((x - mu) / sigma)) ** 2)

bins = range(200)

# Bien approxime par la gaussienne

plot(bins, gaussienne(bins))
hist(x, normed=1)
show()