#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import sqrt, exp
from matplotlib.pyplot import *
from numpy.random import *
from numpy import *
import pickle

data = pickle.load(open('data6.txt', 'r'))

taille_tableau = data.shape

print('Taille de l\'échantillon: %s' % str(taille_tableau))

print(data)

x = data[:,0]
y = data[:,1]

moy_x = sum(x) / float(len(x))

variance_x = (sum(x ** 2) / float(taille_tableau[0])) - moy_x ** 2
ecart_type_x = sqrt(variance_x)

moy_y = sum(y) / float(len(y))

variance_y = (sum(y ** 2) / float(taille_tableau[0])) - moy_y ** 2
ecart_type_y = sqrt(variance_y)

print('Moyenne longueur: %f' % moy_x)
print('Moyenne largeur: %f' % moy_y)

covariance = (sum(x*y) / float(data.shape[0])) - (moy_x * moy_y)

print('Covariance: %f' % covariance)

reg = covariance / (ecart_type_x * ecart_type_y)

print('Coefficient de corrélation linéaire: %f' % reg)

a = float(covariance) / (ecart_type_x ** 2)

droite_reg = lambda x: a*x + (moy_y - a * moy_y)


scatter(x, y)

x = arange(min(x), max(x))

plot(x, droite_reg(x), color='red')
show()

# Ne pas oublier légendes + titre