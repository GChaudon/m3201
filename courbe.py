#!/usr/bin/python

from matplotlib.pyplot import *
from numpy import *


x=arange(-1, 2, 0.01)
plot(x, x**2, 'r-', label='Fonction carre')
plot(x, 2*x-1, 'r:', label='Tangente au point d\'abscisse 1')
legend()
show()