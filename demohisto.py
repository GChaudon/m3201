#!/usr/bin/python

from matplotlib.pyplot import *
from numpy.random import *


mu = 100
sigma = 15
x = mu + sigma * randn(1000)

num_bins = 10

n, bins, patches = hist(x, num_bins, normed=1, facecolor='green', alpha=0.5)

xlabel('Valeur')
ylabel('Probabilite')

title("Histogramme d'une loi gaussienne : micro = {}, sigma={}".format(mu, sigma))

subplots_adjust(left=0.15)
show()