#!/usr/bin/python
# -*- coding utf-8 -*-
from matplotlib.pyplot import *
from numpy.random import *

bar([1,2,3,4,5,6], [60.0, 76.9, 69.0, 76.8, 68.8, 76.6], tick_label=["Africa", "Americas", "South-East Asia", "Europe", "Eastern Mediterranean", "Western Pacific"])
ylabel("Esperance de vie")

title("L'espérance de vie a la naissance en fonction des regions du monde")
show()