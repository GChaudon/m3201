#!/usr/bin/python

import matplotlib.pyplot as plt

labels = ['LREM', 'LR', 'Mouvement democrate', 'Les constructifs', 'Nouvelle Gauche', 'LFI', 'Gauche democrate et republicaine', 'Non Inscrit']
sizes = [314, 100, 47, 35, 31, 17, 16, 16]
explode = (0, 0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.show()