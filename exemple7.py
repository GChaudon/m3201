#!/usr/bin/python

from matplotlib.pyplot import *
from numpy.random import *

bar([1,2,3], [15, 12, 28], tick_label=["VERT", "BLEU", "JAUNE"])
ylabel("nombre d'enfants aimant la couleur")

title("Nombre d'enfants aimant chaque couleur")
show()